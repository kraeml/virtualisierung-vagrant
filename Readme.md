# Anleitung

### Einfache Installation

Mit Default Werten in den Ordner `virtualisierung-vagrant` klonen.

```bash
git clone https://gitlab.com/kraeml/virtualisierung-vagrant.git
cd virtualisierung-vagrant
vagrant up
# vagrant ssh oder was anderes
vagrant halt
cd -
```

## Mit anderen Verzeichnisnamen

Das Fach heißt z.B. `INA`, dann in das Verzeichnis `ina` klonen.

Default Werte werden noch herangezogen.

__Hinweis__ Falls schon ein anderer Gast mit den default Werten läuft, diese zunächst stoppen.

```bash
git clone https://gitlab.com/kraeml/virtualisierung-vagrant.git ina
cd ina
vagrant up
# vagrant ssh oder was anderes
vagrant halt
cd -
```

## Mit eigenen Werten

Das Fach heißt z.B. `BSA_1`, dann in das Verzeichnis `bsa_1` klonen.

Mit der `.env`-Datei können Umgebungsvariablen gesetzt werden.
Wie z.B. `RDF_HOSTNAME` oder `GUEST_IP`.

```bash
git clone https://gitlab.com/kraeml/virtualisierung-vagrant.git bsa_1
cd bsa_1
cp .env.example .env
# Ggf. anpassen.
vagrant up
# vagrant ssh oder was anderes
vagrant halt
cd -
```
